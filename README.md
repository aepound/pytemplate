# New python project.

To get started, clone this repo and start coding in the ./src directory. Write your tests in the ./tests directory.

In order to get the testing to work, you can setup a virtual environment and get the dependencies by:
```
# On unix/linux:
virtualenv -p python3.8 venv
source venv/bin/activate
pip install -r requirements.txt

# On Windows, I needed to do this:
python -m venv venv
./venv/bin/activate
pip install -r requirements.txt
```

Then you should be able to run `pytest` (with no arguments) from the base repo directory and it will run all the tests that it can find.
